import static org.junit.Assert.*;

import org.junit.Test;

public class MenuTests {
    @Test
    public void a_menu_has_starters() {
        Menu menu = new Menu("Starters");
        assertEquals(menu.getTitle(), "Starters");
        menu.getItem().add(new Item("Bruschetta Mozzarella", 4.50));
        assertEquals("Bruschetta Mozzarella", menu.getItem().get(0).getName());
    }

    @Test
    public void a_menu_has_mains() {
        Menu menu = new Menu("Mains");
        assertEquals(menu.getTitle(), "Mains");
        menu.getItem().add(new Item("Spaghetti Bolognese", 11.45));
        assertEquals("Spaghetti Bolognese", menu.getItem().get(0).getName());
    }

    @Test
    public void a_menu_has_desserts() {
        Menu menu = new Menu("Desserts");
        assertEquals(menu.getTitle(), "Desserts");
        menu.getItem().add(new Item("Tiramisu", 7.50));
        assertEquals("Tiramisu", menu.getItem().get(0).getName());
    }
    
    @Test
    public void a_menu_has_drinks() {
        Menu menu = new Menu("Drinks");
        assertEquals(menu.getTitle(), "Drinks");
        menu.getItem().add(new Item("Birra Moretti", 4.00));
        assertEquals("Birra Moretti", menu.getItem().get(0).getName());
    }
}