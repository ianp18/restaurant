import static org.junit.Assert.*;

import org.junit.Test;

public class RestaurantTests {

    @Test
    public void a_restaurant_has_a_name() {
        Restaurant restaurant = new Restaurant("L'Arte dei Sapori");
        assertEquals(restaurant.getRestaurantName(), "L'Arte dei Sapori");
    }

    @Test
    public void a_restaurant_has_a_starters_menu() {
        Restaurant restaurant = new Restaurant("L'Arte dei Sapori");
        Menu menus = new Menu("Starters");
        restaurant.getMenus().add(menus);
        assertEquals(menus, restaurant.getMenus().get(0));
    }

    @Test
    public void a_restaurant_has_a_mains_menu() {
        Restaurant restaurant = new Restaurant("L'Arte dei Sapori");
        Menu menus = new Menu("Mains");
        restaurant.getMenus().add(menus);
        assertEquals(menus, restaurant.getMenus().get(0));
    }

    @Test
    public void a_restaurant_has_a_desserts_menu() {
        Restaurant restaurant = new Restaurant("L'Arte dei Sapori");
        Menu menus = new Menu("Desserts");
        restaurant.getMenus().add(menus);
        assertEquals(menus, restaurant.getMenus().get(0));
    }

    @Test
    public void a_restaurant_has_a_drinks_menu() {
        Restaurant restaurant = new Restaurant("L'Arte dei Sapori");
        Menu menus = new Menu("Drinks");
        restaurant.getMenus().add(menus);
        assertEquals(menus, restaurant.getMenus().get(0));
    }
}