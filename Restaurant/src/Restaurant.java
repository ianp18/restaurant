import java.util.ArrayList;

public class Restaurant {
    private String restaurantName;
    private ArrayList<Menu> menus;

    public Restaurant(String name) {
        this.restaurantName = name;
        this.menus = new ArrayList<Menu>();
    }

    public String getRestaurantName() {
        return restaurantName;
    }

    public ArrayList<Menu> getMenus() {
        return this.menus;
    }
}