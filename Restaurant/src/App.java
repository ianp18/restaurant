import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class App {
    public static void main(String[] args) throws Exception {
        new DB("jdbc:sqlite:./db.sql");
        // PreparedStatement insertRestaurant = DB.conn.
        /*
         * prepareStatement("INSERT INTO restaurants (name, imageURL) VALUES (?, ?);");
         * insertRestaurant.setString(1, "The Longhorn Steakhouse");
         * insertRestaurant.setString(2, "imageURL");
         * insertRestaurant.executeUpdate();
         */

        PreparedStatement insertMenu = DB.conn
                .prepareStatement("INSERT INTO menus (restaurant_id, TITLE) VALUES (?, ?);");
        insertMenu.setInt(1, 3);

        Statement getAllRestaurants = DB.conn.createStatement();
        // PreparedStatement getAllRestaurant = DB.conn.prepareStatement("SELECT * FROM
        // restaurants WHERE id = ?;");
        // PreparedStatement deleteOneRestaurant = DB.conn.prepareStatement("DELETE FROM
        // restaurants WHERE id = ?;");
        try {
            // ResultSet results = getAllRestaurants.executeQuery("SELECT * FROM
            // restaurants;");
            ResultSet results = getAllRestaurants.executeQuery("SELECT * FROM menus;");
            // ResultSet results = getAllRestaurants.executeQuery("SELECT * FROM items;");
            while (results.next()) {
                // System.out.printf(" id: %d\n", results.getInt(1));
                // System.out.printf(" restaurant name: %s\n", results.getString(2));
                // System.out.printf(" imageURL: %s\n", results.getString(3));

                System.out.printf(" menu title: %s\n", results.getString(3));
                System.out.printf(" restaurants name: %s\n", results.getString(1));
                System.out.printf(" restaurant_id: %d\n", results.getInt(2));

                // System.out.printf(" id: %d\n", results.getInt(1));
                // System.out.printf(" menu_id: %d\n", results.getInt(2));
                // System.out.printf(" items name: %s\n", results.getString(3));
                // System.out.printf(" price: %s\n", results.getFloat(4));
            }

        } catch (SQLException error) {

        }

        DB.conn.close();
    }
}