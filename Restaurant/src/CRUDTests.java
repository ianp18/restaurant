import static org.junit.Assert.*;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

import org.junit.Test;

public class CRUDTests {
    @Test
    public void can_connect_ok() throws SQLException {
        new DB("jdbc:sqlite:../db.sql");
        assertTrue(DB.conn instanceof Connection);
        DB.conn.close();
    }

    @Test
    public void db_is_provisioned() throws SQLException {
      new DB("jdbc:sqlite:../db.sql");
      Statement checkSelect = DB.conn.createStatement();
      assert checkSelect.execute("SELECT * FROM restaurants;");
      DB.conn.close();
    }
}
