import static org.junit.Assert.*;

import org.junit.Test;

public class ItemTests {
    @Test
    public void an_item_has_a_name_and_price() {
        Item item = new Item("Bruschetta Mozzarella", 4.50);
        assertEquals("Bruschetta Mozzarella", item.getName());
        assertEquals(item.getPrice(), 4.50, 0);
    }
}